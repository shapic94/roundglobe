# REST server in Node.js

Simple REST server in Node.js with one model (Article).

### Prerequisites

1. You have to install **node** on your machine, advice to latest version.
2. You have to install **mongo** on you machine and start it
```
node -v
npm -v
```

### Installing

First you need to install all packages from package.json

```
npm install
```

Then just start server

```
npm start
```

## API Documentation

Note: There are no things such as pagination and limitation in current version

## Model Article

```
title => String
description => String
published => Integer [0, 1]
version => Integer
```

### GET Articles

Get all articles from database

```
GET http://localhost:3000/api/articles/ 
```

### GET Article History

Get history of changes for current version of article

```
GET http://localhost:3000/api/articles/history/<ARTICLE_ID>/
```

### PUT Article

Create one article

```
PUT http://localhost:3000/api/articles/
BODY 
title:New article
description:Lorem ipsum dolor sit amet
```

### PUT Articles

Create multiple articles

```
PUT http://localhost:3000/api/articles/multiple/
BODY
[
    {
        "title": "New title",
        "description": "New desc"
    },
    {
        "title":"2",
        "description":"laganica bre moj"
    }
]
```


### POST Publish Article

Update current record in database to publish article

```
GET http://localhost:3000/api/articles/publish/<ARTICLE_ID>/
```

### POST Unpublish Article

Update current record in database to unpublish article

```
GET http://localhost:3000/api/articles/unpublish/<ARTICLE_ID>/
```

### POST Article

Create new article with updated fields, and unpublish old

```
GET http://localhost:3000/api/articles/<ARTICLE_ID>/
BODY 
title:New article title
```

### POST Switch Article Version

In database there are records for every update so we can switch version to an older or newer one

```
GET http://localhost:3000/api/articles/<ARTICLE_ID>/version/<VERSION_ID>/
```

### Coding styles

Using eslint with simple rules in this version

```
eslint app/**/*.js
eslint --fix app/**/*.js
```

Errors handling jsonapi.org

## Author

* **Nebojsa Sapic** 

