export class Handler {
    static errorHandle(error) {
        return {
            "errors": [
                {
                    "status": 404,
                    "title": error.name,
                    "detail": error.message
                }
            ]
        }
    }
}