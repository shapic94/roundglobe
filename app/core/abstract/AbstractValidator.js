export class AbstractValidator {
    static validate(query, validateFields) {
        for (let key in query) {
            if (!validateFields.includes(key)) {
                delete query[key];
            }
        }
    }
}
