export class AbstractModel {
    merge(object) {
        for (let key in object) {
            this[key] = object[key];
        }
    }
}