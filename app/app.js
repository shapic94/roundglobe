import {Handler} from "./core/error/Handler";

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(require('./routes'));

app.use(function (request, response, next) {
	Promise.resolve()
		.then(function () {
			response.status(404).json(Handler.errorHandle({
				name: 'Endpoint doesn\'t exist',
				message: ''
			}));
        }).catch(next);
});


app.listen(port, function () {
	console.log('to');
});