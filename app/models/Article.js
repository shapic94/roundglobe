import {AbstractModel} from '../core/abstract/AbstractModel';

const mongoose = require('mongoose');

const ArticleSchema = new mongoose.Schema({
	parent: {
		type: String,
		default: 0
	},
	title: {
		type: String,
		trim: true,
		required: true
	},
	description: {
		type: String,
		trim: true,
		required: true
	},
	published: {
		type: Number,
		min: 0,
		max: 1,
		default: 0
	},
	version: {
		type: Number,
		min: 1,
		default: 1
	},
	createdAt: {
		type: Date,
		default: Date.now
	}
});

class ArticleClass extends AbstractModel {

}

ArticleSchema.loadClass(ArticleClass);

export const Article = mongoose.model('Article', ArticleSchema);