const router = require('express').Router();

router.use('/', require('./ArticleRoutes'));
router.use('/articles', require('./ArticleRoutes'));

router.use(function (error, request, response, next) {
    if ('ValidationError' === error.name) {
        return response.status(500).send('Something went wrong.');
    }

    return next(error);
})

module.exports = router;