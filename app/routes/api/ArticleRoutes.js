import {ArticleController} from "../../controllers/ArticleController";
import {Article} from "../../models/Article";
import {Handler} from "../../core/error/Handler";

const express = require('express');
const router = express.Router();

/**
 * Get Article
 */
router.route('/')
    .get(function (request, response) {
        ArticleController.read(request.query)
            .then(function (article) {
                response.json(article);
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

/**
 * Get Article History
 */
router.route('/history/:articleId')
    .get(function (request, response) {
        const articleId = request.params.articleId;
        ArticleController.read({parent: articleId}, null, {sort: {createdAt: -1}})
            .then(function (article) {
                response.json(article);
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

/**
 * Create Article
 */
router.route('/')
    .put(function (request, response) {
        ArticleController.create(request.body)
            .then(function (article) {
                response.json(article);
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

/**
 * Create Multiple Articles
 */
router.route('/multiple')
    .put(function (request, response) {
        const articlesArray = JSON.parse(request.body.articles);

        Article.insertMany(articlesArray)
            .then(function (articles) {
                response.json(articles)
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

/**
 * Publish Article
 */
router.route('/publish/:articleId')
    .post(function (request, response) {
        ArticleController.update({_id: request.params.articleId}, {published: 1})
            .then(function (success) {
                response.json(success);
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

/**
 * Unpublish Article
 */
router.route('/unpublish/:articleId')
    .post(function (request, response) {
        ArticleController.update({_id: request.params.articleId}, {published: 0})
            .then(function (success) {
                response.json(success);
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

/**
 * Update Article to draft
 * Create new Article
 */
router.route('/:articleId')
    .post(function (request, response) {
        let articleGlobal = null;
        const articleId = request.params.articleId;

        ArticleController.read({_id: articleId})
            .then(function (article) {
                articleGlobal = article[0];
                if (0 === articleGlobal.published) {
                    throw 'This article is unpublished';
                }

                return ArticleController.update({_id: articleId}, {published: 0});
            })
            .then(function (success) {

                request.body['parent'] = '0' === articleGlobal.parent
                    ? articleGlobal._id
                    : articleGlobal.parent;

                request.body['version'] = articleGlobal.version + 1;

                return ArticleController.copy(articleGlobal, request.body);
            })
            .then(function (article) {
                response.json(article);
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

/**
 * Switch version of article
 */
router.route('/:articleId/version/:version')
    .post(function (request, response) {
        const articleId = request.params.articleId;
        const version = request.params.version;

        let parentGlobal = null;
        ArticleController.read({_id: articleId})
            .then(function (article) {
                parentGlobal = '0' === article[0].parent
                    ? article[0]._id
                    : article[0].parent;

                return ArticleController.update(
                    {$or: [
                        {_id: parentGlobal},
                        {parent: parentGlobal}
                    ]},
                    {published: 0},
                    {multi: true}
                );
            })
            .then(function (success) {
                return ArticleController.update(
                    {$or: [
                        {_id: parentGlobal},
                        {parent: parentGlobal}
                    ], version: version},
                    {published: 1}
                );
            })
            .then(function (success) {
                response.json(success);
            })
            .catch(function (error) {
                response.status(400).json(Handler.errorHandle(error));
            });
    });

module.exports = router;