import {AbstractValidator} from '../core/abstract/AbstractValidator';

const getValidateFields = [
	'_id',
	'title',
	'published',
];

const createValidateFields = [
	'title',
	'description',
	'parent',
	'version',
];

const updateValidateFields = [
	'published',
];

export class ArticleValidator extends AbstractValidator {
	static get getValidateFields() {
		return getValidateFields;
	}

	static get createValidateFields() {
		return createValidateFields;
	}

	static get updateValidateFields() {
		return updateValidateFields;
	}
}
