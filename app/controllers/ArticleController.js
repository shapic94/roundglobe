import {Article} from '../models/Article';
import {ArticleValidator} from './ArticleValidator';
import {AbstractController} from '../core/abstract/AbstractController';

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/roundglobe')
	.then(function (success) {
		// continue
    })
	.catch(function (error) {
        response.status(500).send('Something went wrong');
    });

export class ArticleController extends AbstractController {

	static create(object) {
		ArticleValidator.validate(object, ArticleValidator.createValidateFields);

		return Article.create(object);
	}

	static read(object, columns = null, options = null) {
		ArticleValidator.validate(object, ArticleValidator.getValidateFields);

		return Article.find(object, columns, options).exec();
	}

	static update(query, object, options = null) {
		ArticleValidator.validate(object, ArticleValidator.updateValidateFields);

		return Article.update(query, object, options);
	}

	static copy(oldArticle, object) {
		oldArticle.merge(object);

		return this.create(oldArticle.toObject());
	}
}
